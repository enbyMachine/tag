import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


class predPlayer{
	private node node;
	Player alien;
	public predPlayer(node n) {
		this.node = n;
		this.node.setSecCost(0);
		alien = new Player(n);
	}
	List<node> pathfind(){  
		int tentativeGScore;
		node x;
		node current;
		List<node> neighborhood;
		List<node> openSet = new ArrayList<node>();
		List<node> closedSet = new ArrayList<node>();
		openSet.add(this.node);
		while(openSet.size() != 0) {  //while openSet is not empty
			current = openSet.remove(0);	
			 //sort the open set
			if(current.isOccupied()) { //if current = goal
				//print("you found player1. Now you just have to get to them")
				return this.reconstructPath(current);}
			//closedSet.append(openSet.remove(0));
			closedSet.add(current);
			neighborhood = current.getNeighbors();
			for(int i = 0; i < neighborhood.size(); i++) {
				x = neighborhood.get(i); //look at each neighbor
				if(!closedSet.contains(x)) { //if x is not in the closed set, get the gScore
					tentativeGScore = current.getSecCost() + x.getValue();
					if(!openSet.contains(x))//if neighbor not in openSet, Discover a new node
						openSet.add(x);
					openSet.sort(Comparator.comparing(nodeInterface::getFScore));
					if(tentativeGScore <= x.getSecCost()) { //this is weird and I probably need to fix something. //measure for the shortest path from here to start
						x.setPrevious(current);
						x.setSecCost(tentativeGScore);
						x.setSecondaryFScore(x.getSecCost() + x.getHToPrey());}
				}
			}
		}
		return null;
}
	
				
	List<node> reconstructPath(node current) { //build path to allow for selection of a move
		return alien.reconstructPath(current);
	}
	
	void printPath(node[] path) {
		alien.printPath(path);
	}

	int move() { //take the first node from path, move there. check if it's the goal. predplayer could probably try to draw player one closer to it by manipulating swoll and smoll
		List<node> path = this.pathfind();
		if(path.size()==1) {
			this.setNode(path.get(0));} //I think path[0] is the node it's 
		else {
			this.setNode(path.get(1));}
		if(this.getNode().isOccupied()) {
			System.out.println("you ate player 1! om nom nom");
			return 1;
		}
		return 0; //figure out a better return value?
	}
	node getNode() { //return the node that this player is occupying
		return alien.getNode();
		}
	
	void setNode(node n) { //set the node n to the player's node value
		alien.setNode(n);
	}
	
	void testRide() { //test
		alien.testRide();
		}
}
		