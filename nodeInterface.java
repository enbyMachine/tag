import java.util.ArrayList;
import java.util.List;

public interface nodeInterface {
    int value = 0;
    boolean occupied = false;
    double heuristic = -0.1;
    double hToPrey = -0.1;
    int tempV = 0;
    boolean goal = false;
    node cameFrom = null;
    List<node> neighbors = new ArrayList<node>();
    int cost = 0;
    double fScore = -0.1;
    int x = 0;
    int y = 0;
    int secCost = 0;
    double secFScore = -0.1;
    
    int getX();
    
    int getY();
    
    void setX(int n);
    
    void setY(int n);
    
    boolean isOccupied();
    
    void setPrevious(node n);
    
    node getPrevious();
    
    void knockPrevious();
    
    double getFScore();
    
    double getSecondaryFScore();
    
    void setSecondaryFScore(double d);
    
     void setFScore(double d);

    int getValue();

    double getHValue();

    double getHToPrey();

     void setValue(int n);
        
     void addNeighbor(node member);
    
    List<node> getNeighbors();

     void setHValue(double d);

     void setHToPrey(double d);
    
     void occupy();

     void deoccupy();
    
    boolean isGoal();

     void setGoal();
    
    void setTempV(int n);
    
    void smooth();
        
     void setCost(int n);
        
     void setSecCost(int n);
        
    int getSecCost();
        
    int getCost();
}
