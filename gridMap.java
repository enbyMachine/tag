import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
class gridMap {
	/*
	 *  make a NodeData class, you can make an ArrayList<NodeData> and access it in O(1) with the added bonus
	 *  of now having a natural correspondence between nodes and indexes
	 *  Do this in conjunction with an adjacency list
	 * 
	 */

		private List<List<node>> field;
		private int row;
		private int column;
		
		public gridMap(int m, int n) { //set up
			this.row = n;
			this.column = m;
			field = new ArrayList<List<node>>(this.column);
			node temp;
			Random r = new Random();
			for(int x = 0; x < this.row; x++) {
				List<node> xList = new ArrayList<node>(this.column);
				field.add(x, xList);
				for(int y = 0; y < this.column; y++) {
					//build the field
					System.out.print(x);
					System.out.print(y);
					temp = new node(r.nextInt(8)+1);
					field.get(x).add(y, temp); //add the node at x,y
				}
			}
			/** For testing purposes
			 * for(int x = 0; x < this.row; x++) {
				System.out.println();
				for(int y = 0; y < this.column; y++) {
					System.out.print(field.get(x).get(y).getValue());
				}
			}*/
		}
		void build() {
			node z;
			for(int i = 0; i < this.field.size(); i++) {
				for(int c = 0; c < this.field.get(i).size(); c++) {
					z = this.field.get(i).get(c);
					if(i == 0 & c == 0) {
						z.setGoal();
					}
					z.setX(c); //coordinates
					z.setY(i);
					z.setHValue(Math.sqrt(Math.pow(0-i, 2)+Math.pow(0-c, 2))); //heuristic for p1
					//z.setHValue(Math.sqrt(((0-i)**2)+((0-c)**2))); //set the heuristic for p1
				}
			}
			this.smooth();
		 }
		 
		void edit() { //allow edits build this in later, get it working first (It will need to display the nodes in a map)
			//get input from user, and then get that node and change the value
			int a = -1;
			int b = -1;
			int c = -1;
			int d = -1;
			Scanner scanner = new Scanner(System.in);
			this.toString();
			while((a < 0 || a > this.field.size()) != false) {
				System.out.println("Give me the column index"); //add try catch
				a = scanner.nextInt();
				//print(a)
			}
			while((b < 0 || b > this.field.get(0).size()) != false) {
				System.out.println("Give me the row index"); //add try catch
				b = scanner.nextInt();
			}
			while((c < 1 || c > 5)) { //allow dynamic radius based on total map size
				System.out.println("What length of radius to you want to impact? (between 1 and 5)"); //add try catch
				c = scanner.nextInt();
			}
			//while((d < 0 || d > 9) & isinstance(d, int) != false) {
			while((d < 0 || d > 9)) {// & d instanceof Integer) {
				System.out.println("Enter a value, between 0 and 9, with which to replace it"); //add try catch
				d = scanner.nextInt();
			}
			scanner.close();
			node z = this.field.get(a).get(b);
			int l = z.getValue();
			z.setValue(d);
			if(l > d)
				{this.smoll(a, b, c, d);}
			else
				{this.swoll(a, b, c, d);}
				
			this.toString();
		}
		
		private void smooth() {
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++) {
					if(i==0) { //first row
						if(j==0) { //beginning of first row
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/3); //get average of node values repeat all the way down
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
						else if(j==this.field.get(i).size() - 1){ //end of first row
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i+1).get(j).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						}
						else { //first row, neither first nor last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
							this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
					}
					else if(i == this.field.size()-1) { //last row
						if(j==0) { //last row first column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
						else if(j == this.field.get(i).size()-1) { //last row last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue())/3);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						}
						else { //last row and neither first nor last column
							this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
							this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
							this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						}
					} //this is the one if it throws an  error later
					else if(j==0 & i != 0 & i != this.field.size()-1) { //first column and neither first row nor last row
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
					}
					else if(j==this.field.get(i).size()-1 & i != 0 & i != this.field.size()-1) { //last column but not top or bottom row
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j).getValue() +this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j-1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
					}
					else {	//middle nodes
						this.field.get(i).get(j).setTempV((this.field.get(i).get(j-1).getValue() + this.field.get(i-1).get(j).getValue() + this.field.get(i+1).get(j).getValue() + this.field.get(i).get(j+1).getValue())/4);
						this.field.get(i).get(j).addNeighbor(this.field.get(i+1).get(j));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j+1));
						this.field.get(i).get(j).addNeighbor(this.field.get(i).get(j-1));
						this.field.get(i).get(j).addNeighbor(this.field.get(i-1).get(j));
					}
					}
				}
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					this.field.get(i).get(j).smooth();} //switching value for temp value
			}
		}
		
		
		void iterateValue(node pNode) {
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){ {
					this.field.get(i).get(j).setHToPrey(Math.sqrt(Math.pow(pNode.getX()-j,2)+Math.pow(pNode.getY()-i,2))); //heuristic for p2
					//this.field.get(i).get(j).setHToPrey(Math.sqrt(((pNode.getX()-j)**2)+((pNode.getY()-i)**2))); //heuristic for p2
				}
			}
		}
		}
		
		void reset() {
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){ {
					this.field.get(i).get(j).knockPrevious();}  //reset path; prevents standstill
					//this.field.get(i).get(j).setCost(9999)
					//this.field.get(i).get(j).setFScore(0)
			}
		}
		}
						
		node randNode() {  //return a random node place p 1
			Random r = new Random();
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					if(r.nextInt(10000) > 9900)
						{return this.field.get(i).get(j);}
				}
			}
			return this.field.get(this.field.size()-1).get(this.field.get(0).size()-1);
		}
		
		node randBad() {  //return a random node place p2
			Random r = new Random();
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					if(r.nextInt(10000) > 7085) { //make this dynamic
						return this.field.get(i).get(j);}
				}
			}
			return this.field.get((int) Math.floor(this.field.size()/2)).get((int) Math.floor(this.field.size()/2));
		//math might be wrong
		}
		
						
		/*#def search(this, centerx, centery, radius):
			#for i in range(centerx - radius, centerx + radius):
				#for j in range(centery - radius, centery + radius):
					#if(i < 0 or j < 0):
						#break
					#if(i > len(this.field) or j > len(this.field[i])):
						#break
					#if(i == centery or j == centerx):
						#break
					#if(this.field.get(i).get(j).isOccupied == true):
						#return true
		//get the position of a player and search within the radius for other players
		 * 
		 */
						
		void swoll( int centerx, int centery, int radius, int amount) { //swell an area
			for(int i = (int) Math.ceil(centerx - radius); i < Math.ceil(centerx + radius); i++) {//check whether, in python, the range for for loops is inclusive or exclusive also check your cast to int: does it go up or down
				for(int j = (int) Math.ceil(centery - radius); j < Math.ceil(centery + radius); j++) {//check whether, in python, the range for for loops is inclusive or exclusive also check your cast to int: does it go up or down
					if(i < 0 || j < 0)
						{break;}
					if(i > this.field.size() || j > this.field.get(i).size())
						{break;}
					this.field.get(i).get(j).setValue(Math.min(9,this.field.get(i).get(j).getValue()+(amount-(Math.max(i,j)/2))));
				}
			}	
		}
					
		
		void smoll(int centerx, int centery, int radius, int amount) { //negatively swell an area
			for(int i = (int) Math.ceil(centerx - radius); i < Math.ceil(centerx + radius); i++) {
				for(int j = (int) Math.ceil(centery - radius); j < Math.ceil(centery + radius); j++) {
					if(i < 0 || j < 0)
						break;
					if(i > this.field.size() || j > this.field.get(i).size())
						break;
					this.field.get(i).get(j).setValue(Math.max(0,this.field.get(i).get(j).getValue()-(amount+(Math.max(i,j)*2)))); 
				}
			}
		}
		
		public void print() { //print the thing
			String a = "  " ;
			String p = "";
			for(int b = 0; b < this.field.size(); b++) {
				a += String.valueOf(b)+ "_";
			}
			System.out.println(a);
			for(int i = 0; i < this.field.size(); i++) {
				p = String.valueOf(i) + "|";
				for(int j = 0; j < this.field.get(i).size(); j++) {
					p += " "+ String.valueOf((Math.floor(this.field.get(i).get(j).getValue())));}
				System.out.println(p);
				System.out.println("");
			}
		}
				
		void truth() {  //make sure that only the goal is the goal
			for(int i = 0; i < this.field.size(); i++) {
				for(int j = 0; j < this.field.get(i).size(); j++){
					if(this.field.get(i).get(j).isGoal()) {
						System.out.println("1");}
					else {
						System.out.println("0");}
				}
			}
		}
						
		void countNeighbors() { //count neighbors of each node
			for(int i = 0; i < this.field.size(); i++) {
				String x = "";
				for(int j = 0; j < this.field.get(i).size(); j++){
					x += " "+ (this.field.get(i).get(j).getNeighbors().size());
				}
				System.out.println(x);
			}
		}
			               
			
			
		void testRide(){
			this.toString();
			System.out.println("testing build");
			this.build();
			System.out.println("truth table");
			this.truth();
			System.out.println("testing edit");
			this.edit();
			System.out.println("testing swoll");
			this.swoll(3, 3, 4, 3);
			this.toString();
			System.out.println("testing smoll");
			this.smoll(3, 3, 4, 3);
			this.toString();
			System.out.println("testing the function to return a random node");
			node x = this.randNode();
			System.out.println(x.getX());
			System.out.println(x.getY());
			System.out.println("testing the function to find distances between two nodes");
			this.iterateValue(this.randNode());
			this.countNeighbors();
		 }
	
		
		/**#def smooth(xStart, yStart, xEnd, yEnd): #traverse and smooth. math.ceil(x) returns the highest int value closest to a decimal
			#i = xStart
			#n = yStart
			#for i in range(xEnd):
				#for n in range(yEnd): #I fucked up global variables
					#z = field[i][n]
		*/
}		


